﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {

            List<string> List = new List<string>();
            List.Add("Sandra");
            List.Add("Rui");
            List.Add("sofia");
            List.Add("Salomé");
            IEnumerable names = from n in List where (n.StartsWith("S")) select n;
            foreach (string name in names)
            {
                Console.WriteLine(name);
            }
            Console.ReadLine();
        }
    }
}
